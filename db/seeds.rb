# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)


articles = [
  {
    title: 'hello world',
    content: 'welcome to',
    slug: 'hello-world'
  },
  {
    title: 'Ruby On Rails',
    content: 'Developers tool',
    slug: 'ruby-on-rails'
  },
  {
    title: 'Backend devs',
    content: 'Must read',
    slug: 'backend-devs'
  }
]

articles.each do |article|
  Article.create(article)
end

puts 'Seeds Created'
